% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 793.307918828693005 ; 791.570493285914949 ];

%-- Principal point:
cc = [ 507.873414782546377 ; 381.964008728571969 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.229326858890067 ; -0.873750192681530 ; 0.003257543571739 ; -0.002005742298365 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 1.830959828985873 ; 1.800506964115921 ];

%-- Principal point uncertainty:
cc_error = [ 2.450516673970424 ; 2.353580497610614 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.014734733038567 ; 0.073497856224442 ; 0.001335732366101 ; 0.001351670084363 ; 0.000000000000000 ];

%-- Image size:
nx = 1024;
ny = 768;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 19;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.088200e+00 ; -2.124407e+00 ; -2.123678e-01 ];
Tc_1  = [ -8.729319e+01 ; -4.212968e+01 ; 2.215831e+02 ];
omc_error_1 = [ 2.863763e-03 ; 3.367369e-03 ; 6.533295e-03 ];
Tc_error_1  = [ 6.835343e-01 ; 6.771716e-01 ; 6.780847e-01 ];

%-- Image #2:
omc_2 = [ -1.635498e+00 ; -1.604612e+00 ; -8.647533e-01 ];
Tc_2  = [ -7.420632e+01 ; -2.662746e+01 ; 2.396749e+02 ];
omc_error_2 = [ 2.413421e-03 ; 3.219669e-03 ; 3.935409e-03 ];
Tc_error_2  = [ 7.445427e-01 ; 7.255099e-01 ; 6.754407e-01 ];

%-- Image #3:
omc_3 = [ -1.876180e+00 ; -1.254687e+00 ; -8.006064e-01 ];
Tc_3  = [ -9.550687e+01 ; 1.234875e+01 ; 2.758578e+02 ];
omc_error_3 = [ 2.759536e-03 ; 3.020456e-03 ; 3.878513e-03 ];
Tc_error_3  = [ 8.516836e-01 ; 8.324096e-01 ; 7.457943e-01 ];

%-- Image #4:
omc_4 = [ -1.474824e+00 ; -1.858494e+00 ; -1.200224e+00 ];
Tc_4  = [ -3.514153e+01 ; -2.407215e+01 ; 1.972261e+02 ];
omc_error_4 = [ 2.171797e-03 ; 3.588127e-03 ; 4.018830e-03 ];
Tc_error_4  = [ 6.161281e-01 ; 5.946278e-01 ; 6.179996e-01 ];

%-- Image #5:
omc_5 = [ 1.931157e+00 ; 1.866293e+00 ; 1.417212e+00 ];
Tc_5  = [ -2.965771e+01 ; -2.598112e+01 ; 1.935136e+02 ];
omc_error_5 = [ 3.806288e-03 ; 2.205338e-03 ; 4.597557e-03 ];
Tc_error_5  = [ 6.089540e-01 ; 5.818126e-01 ; 6.243230e-01 ];

%-- Image #6:
omc_6 = [ 1.665102e+00 ; 1.827113e+00 ; 2.556396e-01 ];
Tc_6  = [ -4.789304e+01 ; -6.346278e+01 ; 2.644221e+02 ];
omc_error_6 = [ 2.832471e-03 ; 2.788628e-03 ; 4.599518e-03 ];
Tc_error_6  = [ 8.211459e-01 ; 7.859469e-01 ; 7.035899e-01 ];

%-- Image #7:
omc_7 = [ 1.772786e+00 ; 1.862092e+00 ; 1.221205e-01 ];
Tc_7  = [ -7.536023e+01 ; -5.667800e+01 ; 2.814173e+02 ];
omc_error_7 = [ 2.860791e-03 ; 3.043034e-03 ; 5.209915e-03 ];
Tc_error_7  = [ 8.778454e-01 ; 8.444792e-01 ; 7.528461e-01 ];

%-- Image #8:
omc_8 = [ 1.999249e+00 ; 2.008523e+00 ; -3.871468e-01 ];
Tc_8  = [ -8.149406e+01 ; -5.950811e+01 ; 2.806054e+02 ];
omc_error_8 = [ 2.691629e-03 ; 3.252846e-03 ; 5.722615e-03 ];
Tc_error_8  = [ 8.683495e-01 ; 8.388031e-01 ; 6.633084e-01 ];

%-- Image #9:
omc_9 = [ -2.048642e+00 ; -1.892481e+00 ; 8.827635e-01 ];
Tc_9  = [ -1.005492e+02 ; -4.524809e+01 ; 3.023708e+02 ];
omc_error_9 = [ 3.479415e-03 ; 2.222278e-03 ; 4.797601e-03 ];
Tc_error_9  = [ 9.441905e-01 ; 9.189702e-01 ; 6.121612e-01 ];

%-- Image #10:
omc_10 = [ -1.917630e+00 ; -1.670042e+00 ; 9.834702e-01 ];
Tc_10  = [ -6.860161e+01 ; -3.044065e+01 ; 3.350618e+02 ];
omc_error_10 = [ 3.267730e-03 ; 2.247213e-03 ; 4.335428e-03 ];
Tc_error_10  = [ 1.031426e+00 ; 9.973810e-01 ; 5.876204e-01 ];

%-- Image #11:
omc_11 = [ -1.922548e+00 ; -1.893448e+00 ; 2.370987e-01 ];
Tc_11  = [ -9.846633e+01 ; -3.347775e+01 ; 2.851932e+02 ];
omc_error_11 = [ 2.850065e-03 ; 2.918892e-03 ; 5.172741e-03 ];
Tc_error_11  = [ 8.799958e-01 ; 8.612131e-01 ; 6.658307e-01 ];

%-- Image #12:
omc_12 = [ -1.755686e+00 ; -1.655466e+00 ; -5.460687e-01 ];
Tc_12  = [ -8.478146e+01 ; 2.194962e+01 ; 2.582167e+02 ];
omc_error_12 = [ 2.466744e-03 ; 3.324115e-03 ; 4.380611e-03 ];
Tc_error_12  = [ 7.950877e-01 ; 7.775208e-01 ; 6.818345e-01 ];

%-- Image #13:
omc_13 = [ -2.081994e+00 ; -1.781742e+00 ; -1.146157e+00 ];
Tc_13  = [ -3.773106e+01 ; 1.214671e+01 ; 2.083951e+02 ];
omc_error_13 = [ 2.263395e-03 ; 3.633564e-03 ; 4.846076e-03 ];
Tc_error_13  = [ 6.466419e-01 ; 6.244617e-01 ; 6.184728e-01 ];

%-- Image #14:
omc_14 = [ -1.712686e+00 ; -1.756726e+00 ; -8.044352e-01 ];
Tc_14  = [ -7.044922e+01 ; -2.039254e+01 ; 2.395250e+02 ];
omc_error_14 = [ 2.318248e-03 ; 3.277963e-03 ; 4.262039e-03 ];
Tc_error_14  = [ 7.422720e-01 ; 7.242199e-01 ; 6.800074e-01 ];

%-- Image #15:
omc_15 = [ 1.753662e+00 ; 1.838572e+00 ; -6.631857e-01 ];
Tc_15  = [ -7.430167e+01 ; -8.447294e+01 ; 3.388338e+02 ];
omc_error_15 = [ 2.408871e-03 ; 3.324099e-03 ; 4.640491e-03 ];
Tc_error_15  = [ 1.057684e+00 ; 1.017166e+00 ; 7.408150e-01 ];

%-- Image #16:
omc_16 = [ 1.669455e+00 ; 1.629928e+00 ; -1.854903e-01 ];
Tc_16  = [ -5.903034e+01 ; -6.881413e+01 ; 3.222034e+02 ];
omc_error_16 = [ 2.718718e-03 ; 2.970983e-03 ; 4.317541e-03 ];
Tc_error_16  = [ 9.972720e-01 ; 9.584131e-01 ; 7.975652e-01 ];

%-- Image #17:
omc_17 = [ 2.005886e+00 ; 2.063001e+00 ; -1.026115e+00 ];
Tc_17  = [ -6.579231e+01 ; -7.110046e+01 ; 3.744934e+02 ];
omc_error_17 = [ 2.510251e-03 ; 3.626831e-03 ; 5.324284e-03 ];
Tc_error_17  = [ 1.167500e+00 ; 1.115445e+00 ; 7.403631e-01 ];

%-- Image #18:
omc_18 = [ -1.615765e+00 ; -1.616104e+00 ; 7.787697e-01 ];
Tc_18  = [ -6.590106e+01 ; -6.467283e+01 ; 3.235633e+02 ];
omc_error_18 = [ 3.143682e-03 ; 2.500170e-03 ; 3.875090e-03 ];
Tc_error_18  = [ 1.005128e+00 ; 9.652544e-01 ; 6.456511e-01 ];

%-- Image #19:
omc_19 = [ 1.829841e+00 ; 1.924240e+00 ; 8.410568e-01 ];
Tc_19  = [ -1.307797e+01 ; -5.442051e+01 ; 1.965063e+02 ];
omc_error_19 = [ 3.356643e-03 ; 2.450516e-03 ; 4.637786e-03 ];
Tc_error_19  = [ 6.164691e-01 ; 5.796098e-01 ; 5.816568e-01 ];

