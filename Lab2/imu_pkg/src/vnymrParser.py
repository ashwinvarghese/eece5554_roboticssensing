# $GPGGA,,,,,,0,00,99.99,,,,,,*48
# Borrowed from https://github.com/ros-drivers/nmea_navsat_driver/blob/master/src/libnmea_navsat_driver/parser.py


import re
import datetime
import calendar
import math
import logging



def parse_vnymr_sentence(vnymr_sentence):
    """Parse a VNYMR sentence string into a dictionary.
    Args:
        nmea_sentence (str): A single NMEA sentence of one of the types in parse_maps.
    Returns:
        A dict mapping string field names to values for each field in the NMEA sentence or
        False if the sentence could not be parsed.
    """
    # Check for a valid nmea sentence

    fields = [field.strip(',') for field in vnymr_sentence.split(',')]

    sentence_type = "VNYMR"

    parse_map = message_map[sentence_type]

    parsed_sentence = {}
    for entry in parse_map:
        try:
            parsed_sentence[entry[0]] = entry[1](fields[entry[2]])
        except IndexError:
            parsed_sentence[entry[0]] = float('NaN')

    return {sentence_type: parsed_sentence}

def safe_float(field):
    """Convert  field to a float.
    Args:
        field: The field (usually a str) to convert to float.
    Returns:
        The float value represented by field or NaN if float conversion throws a ValueError.
    """
    try:
        return float(field.split("*")[0])
    except ValueError:
        return float('NaN')

message_map = { 
    "VNYMR": [
        ("yaw", safe_float, 1),
        ("pitch", safe_float, 2),
        ("roll", safe_float, 3),
        ("mag_x", safe_float, 4),
        ("mag_y", safe_float, 5),
        ("mag_z", safe_float, 6),
        ("accel_x", safe_float, 7),
        ("accel_y", safe_float, 8),
        ("accel_z", safe_float, 9),
        ("ang_x", safe_float, 10),
        ("ang_y", safe_float, 11),
        ("ang_z", safe_float, 12),
    ]
}


# print parse_vnymr_sentence("$VNYMR,+166.948,-006.017,-030.909,-00.1854,-00.2541,+00.4005,-01.040,+04.993,-08.094,+00.011247,+00.001359,+00.002484*60")