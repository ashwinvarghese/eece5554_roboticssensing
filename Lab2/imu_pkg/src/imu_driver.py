#!/usr/bin/env python

# Adapted from https://github.com/ros-drivers/nmea_navsat_driver/blob/master/src/libnmea_navsat_driver/nodes/nmea_topic_serial_reader.py

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, Eric Perko
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the names of the authors nor the names of their
#    affiliated organizations may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import math
import serial

# from nmea_msgs.msg import Sentence
import rospy
import vnymrParser as vnymrParser
import utm


from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from std_msgs.msg import String

from tf.transformations import quaternion_from_euler


# # from libnmea_navsat_driver.driver import RosNMEADriver
# from puck_parser.msg import UTMSentence
 

def main():
    rospy.init_node('vynmr_topic_serial_reader')

    vnymr_pub_raw = rospy.Publisher("vynmr_sentence_raw", String, queue_size=1)
    vnymr_pub_imu = rospy.Publisher("vynmr_sentence_imu", Imu, queue_size=1)
    vnymr_pub_mf = rospy.Publisher("vynmr_sentence_mf", MagneticField, queue_size=1)

    serial_port = rospy.get_param('~port', '/dev/ttyUSB0')
    serial_baud = rospy.get_param('~baud', 115200)

    try:
        IMU = serial.Serial(port=serial_port, baudrate=serial_baud, timeout=2)
        while not rospy.is_shutdown():
            parsed_sentence = {}

            unparsed_vnymr_sentence = IMU.readline().strip()
            # unparsed_vnymr_sentence = "$VNYMR,+166.948,-006.017,-030.909,-00.1854,-00.2541,+00.4005,-01.040,+04.993,-08.094,+00.011247,+00.001359,+00.002484*60"
            
            if (unparsed_vnymr_sentence.startswith("$VNYMR")):
            	vnymr_pub_raw.publish(unparsed_vnymr_sentence)

            	parsed_sentence = vnymrParser.parse_vnymr_sentence(unparsed_vnymr_sentence)

            	data = parsed_sentence['VNYMR']

            	q = quaternion_from_euler(math.radians(data["roll"]), math.radians(data["pitch"]), math.radians(data["yaw"]))
            	msg1 = Imu()

            	msg1.header.stamp = rospy.Time.now()

            	msg1.orientation.x = q[0]
            	msg1.orientation.y = q[1]
            	msg1.orientation.z = q[2]
            	msg1.orientation.w = q[3]

            	msg1.angular_velocity.x = data["ang_x"]
            	msg1.angular_velocity.y = data["ang_y"]
            	msg1.angular_velocity.z = data["ang_z"]

            	msg1.linear_acceleration.x = data["accel_x"]
            	msg1.linear_acceleration.y = data["accel_y"]
            	msg1.linear_acceleration.z = data["accel_z"]

            	msg2 = MagneticField()

            	msg2.header.stamp = rospy.Time.now()

            	msg2.magnetic_field.x = data["mag_x"]
            	msg2.magnetic_field.y = data["mag_y"]
            	msg2.magnetic_field.z = data["mag_z"]

            	vnymr_pub_imu.publish(msg1)
            	vnymr_pub_mf.publish(msg2)

    except rospy.ROSInterruptException:
        GPS.close()  # Close GPS serial port

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass