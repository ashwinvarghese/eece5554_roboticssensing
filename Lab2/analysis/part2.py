import matplotlib.pyplot as pl
import csv
import numpy as np
import math

from analysis import yaw_complementary_result

from scipy import integrate
from scipy import signal

from tf.transformations import euler_from_quaternion


x = []
y = []
lin_ax = []
lin_ay = []
gyro_z = []
easting = []
northing = []
sample_points_gps = []
sample_points = []


with open('imu.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        lin_ax.append(float(row[29]))
        lin_ay.append(float(row[30]))
        gyro_z.append(float(row[19]))
        sample_points.append(long(row[0]))

with open('GPS.txt','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        easting.append(float(row[7]))
        northing.append(float(row[8]))
        sample_points_gps.append(int(row[0]))



relevant_lin_ax = lin_ax[2700:]
lin_ax_no_bias = np.subtract(relevant_lin_ax, 0.034254)

# integrated for velocity
fw_vel_integrated_lin_ax = integrate.cumtrapz(lin_ax_no_bias, sample_points[2700:])
fw_vel_integrated_lin_ax_scaled = fw_vel_integrated_lin_ax / 10**10
sample_imu_times = sample_points[2700:]
standardized_sample_imu_times = np.divide(np.subtract(np.array(sample_imu_times), sample_imu_times[0]), 10**9)


pl.figure()
pl.plot(standardized_sample_imu_times[:-1], fw_vel_integrated_lin_ax_scaled, label="Integrated Forward Acceleration (Estimated)")

# differentiated gps for velocity
pos_x = easting[66:]
pos_y = northing[66:]
sample_gps_times  = sample_points_gps[66:]
standardized_sample_gps_times = np.divide(np.subtract(np.array(sample_gps_times), sample_gps_times[0]), 10**9)

gps_velocities = []

for i in range(len(pos_x) -1):
    delta_x = abs(pos_x[i] - pos_x[i+1])
    delta_y = abs(pos_y[i] - pos_y[i+1])
    delta_t = abs(sample_gps_times[i] - sample_gps_times[i+1])
    gps_velocity = math.sqrt(delta_x**2.0 + delta_y**2.0) / delta_t * 10**9
    gps_velocities.append(gps_velocity)

pl.plot(standardized_sample_gps_times[:-1], gps_velocities, label="Differentiated GPS Position (Ground truth)")
pl.title('Raw Calculated Velocity vs GPS Velocity (m/s)')
pl.xlabel('Time (s)')
pl.ylabel('Velocity (m/s)')
pl.legend()


# manipulating linear accel (found ranges by seeing low constant acceleration in original)
# 1340 - 2400
# 4150 - 6000
# 7200 - 8554
# 9845 - 10685
# 15165 - 15840
# 17760 - 18405
# 21700 - 22165
# 23250 - 23588
# 25625 - 25823

lin_ax_no_bias_altered = np.array(lin_ax_no_bias)
lin_ax_no_bias_altered[1340:2400] = 0
lin_ax_no_bias_altered[4150:6000] = 0
lin_ax_no_bias_altered[7200:8554] = 0
lin_ax_no_bias_altered[9845:10685] = 0
lin_ax_no_bias_altered[15165:15840] = 0
lin_ax_no_bias_altered[17760:18405] = 0
lin_ax_no_bias_altered[21700:22165] = 0
lin_ax_no_bias_altered[23250:23588] = 0
lin_ax_no_bias_altered[25625:25823] = 0

adjusted_velocities = []


adjusted_velocities2 = integrate.cumtrapz(lin_ax_no_bias_altered - np.mean(lin_ax_no_bias_altered) , standardized_sample_imu_times)


for i in range(len(lin_ax_no_bias_altered) - 1):
    delta_a = abs(lin_ax_no_bias_altered[i] - lin_ax_no_bias_altered[i+1])
    delta_t = abs(sample_imu_times[i] - sample_imu_times[i+1])
    imu_velocity = delta_a * delta_t
    adjusted_velocities.append(imu_velocity)


pl.figure()
pl.title("Corrected Velocity vs GPS Velocity (m/s)")
pl.plot(standardized_sample_imu_times[:-1], np.divide(adjusted_velocities, 10**7), label="Corrected Estimated Velocity(m/s)")
pl.plot(standardized_sample_gps_times[:-1], gps_velocities, label="Differentiated GPS Position (Ground truth)")
pl.xlabel('Time (s)')
pl.ylabel('Velocity (m/s)')
pl.legend()


# PART 3

omega = gyro_z[2701:]
computed_y_acc = fw_vel_integrated_lin_ax_scaled * omega
relevant_lin_ay = lin_ay[2701:]
y_acc = np.array(relevant_lin_ay) - lin_ay[0] - .0255007
pl.figure()
pl.plot(computed_y_acc, label='Computed y-acceleration')
pl.xlabel("Sample point number")
pl.ylabel('Acceleration (m/s^2)')
pl.title("Comparing wX' and y''")




sampling = 40.0
nyquist = sampling / 2
cutoff = .5
critical =  cutoff / nyquist
b, a = signal.butter(1, critical, 'low')
low_filtered_result = signal.filtfilt(b, a, y_acc)
# pl.figure()
pl.plot(low_filtered_result, label='Observed y-acceleration')
pl.legend()


# print (y_acc - computed_y_acc) / (np.diff(omega, append=0) # 0.16744159992748103 (x_c calculation)

x = []
y = []
lin_ax = []
easting = []
northing = []
sample_points_gps = []

with open('GPS.txt','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        easting.append(float(row[7]))
        northing.append(float(row[8]))
        sample_points_gps.append(int(row[0]))


# Compare wX and y observed acceleration


# GPS displacement

pl.figure()
easting_displacement = np.subtract(easting, easting[0])
northing_displacement = np.subtract(northing, northing[0])
pl.plot(easting_displacement[66:600], northing_displacement[66:600])

# IMU displacement  
q_x = []
q_y = []
q_z = []
q_w = []

raw_yaw = []

with open('Orientation_driving.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        q_x.append(float(row[1]))
        q_y.append(float(row[2]))
        q_z.append(float(row[3]))
        q_w.append(float(row[4]))

for i in range(len(q_x)):
    ro, pi, ya = euler_from_quaternion([q_x[i], q_y[i], q_z[i], q_w[i]])
    raw_yaw.append(ya)


x_obs = np.multiply(adjusted_velocities2, np.cos((raw_yaw[2701:])))
y_obs = np.multiply(adjusted_velocities2, np.sin((raw_yaw[2701:])))

x_disp = []
y_disp=[]


# calculate new position
prev_x = 0
prev_y = 0
x_disp.append(prev_x)
y_disp.append(prev_y)
for i in range(len(x_obs)):
    if i > 0:
        delta_t = sample_imu_times[i] - sample_imu_times[i-1];
        x_pos = prev_x + x_obs[i]*delta_t
        y_pos = prev_y + y_obs[i]*delta_t
        prev_x = x_pos
        prev_y = y_pos
        x_disp.append(x_pos)
        y_disp.append(y_pos)

pl.figure()

x_disp_scaled = np.divide(x_disp, 10**9)
y_disp_scaled = np.divide(y_disp, 10**9)
pl.plot(x_disp_scaled, y_disp_scaled)


theta = 3*math.pi / 2 # math.pi
c, s = np.cos(theta), np.sin(theta)
r = np.array(((c, s), (-s, c)))
rotated_x, rotated_y = r.dot([x_disp_scaled, y_disp_scaled])

pl.figure()
pl.plot(easting_displacement[66:600], northing_displacement[66:600], label="GPS Displacement")
pl.plot(rotated_x, rotated_y, label="IMU Displacement")
pl.title("GPS Trajectory vs IMU Trajectory")
pl.xlabel("Displacement from origin in x (m)")
pl.ylabel("Displacement from origin in y (m)")
pl.legend()

pl.show()
