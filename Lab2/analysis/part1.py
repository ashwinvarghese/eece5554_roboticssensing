
import matplotlib.pyplot as pl
import csv
import numpy as np

from scipy import integrate

yaw = []
pitch = []
roll = []
gyro_x = []
gyro_y = []
gyro_z = []
lin_acc_x = []
lin_acc_y = []
lin_acc_z = []
mag_x = []
mag_y = []
mag_z = []

with open('raw_stat.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line isfield names
    for row in plots:
        yaw.append(float(row[2]))
        pitch.append(float(row[3]))
        roll.append(float(row[4]))

with open('stationary_imu.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        gyro_x.append(float(row[17]))
        gyro_y.append(float(row[18]))
        gyro_z.append(float(row[19]))
        lin_acc_x.append(float(row[29]))
        lin_acc_y.append(float(row[30]))
        lin_acc_z.append(float(row[31]))

with open('stationary_mf.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        mag_x.append(float(row[4]))
        mag_y.append(float(row[5]))
        mag_z.append(float(row[6]))


trimmed_yaw = yaw[:3000]
trimmed_pitch = pitch[:3000]
trimmed_roll = roll[:3000]

trimmed_gyro_x =gyro_x[:3000]
trimmed_gyro_y = gyro_y[:3000]
trimmed_gyro_z = gyro_z[:3000]

trimmed_lin_acc_x = lin_acc_x[:3000]
trimmed_lin_acc_y = lin_acc_y[:3000]
trimmed_lin_acc_z = lin_acc_z[:3000]

trimmed_mag_x = mag_x[:3000]
trimmed_mag_y = mag_x[:3000]
trimmed_mag_z = mag_x[:3000]

# ypr plots and calculations

pl.figure()
pl.plot(trimmed_yaw)
pl.hlines(np.mean(trimmed_yaw), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_yaw)) + 'degrees')
pl.title('Raw yaw over time' + '\n stdev=' + str(np.std(trimmed_yaw))  + 'degrees')
pl.xlabel('Point number')
pl.ylabel('Yaw (degrees)')
pl.legend()

pl.figure()
pl.hist(trimmed_yaw - np.mean(trimmed_yaw))
pl.title('Histogram of yaw frequencies')
pl.xlabel('Yaw (degrees)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of yaw")


pl.figure()
pl.plot(trimmed_pitch)
pl.hlines(np.mean(trimmed_pitch), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_pitch)) + 'degrees')
pl.title('Raw pitch over time' + '\n stdev=' + str(np.std(trimmed_pitch))  + 'degrees')
pl.xlabel('Point number')
pl.ylabel('Pitch (degrees)')
pl.legend()

pl.figure()
pl.hist(trimmed_pitch - np.mean(trimmed_pitch))
pl.title('Histogram of pitch frequencies')
pl.xlabel('Pitch (degrees)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of pitch")


pl.figure()
pl.plot(trimmed_roll)
pl.hlines(np.mean(trimmed_roll), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_roll)) + 'degrees')
pl.title('Raw roll over time' + '\n stdev=' + str(np.std(trimmed_roll))  + 'degrees')
pl.xlabel('Point number')
pl.ylabel('Roll (degrees)')
pl.legend()

pl.figure()
pl.hist(trimmed_roll - np.mean(trimmed_roll))
pl.title('Histogram of roll frequencies')
pl.xlabel('Roll (degrees)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of roll")




# gyro plots and calculations

pl.figure()
pl.plot(trimmed_gyro_x)
pl.hlines(np.mean(trimmed_gyro_x), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_gyro_x)) + 'radians/sec')
pl.title('Raw gyro_x over time' + '\n stdev=' + str(np.std(trimmed_gyro_x))  + 'radians/sec')
pl.xlabel('Point number')
pl.ylabel('Gyro_x (radians/sec)')
pl.legend()

pl.figure()
pl.hist(trimmed_gyro_x - np.mean(trimmed_gyro_x))
pl.title('Histogram of gyro_x')
pl.xlabel('Gyro_x (radians/sec)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of gyro_x")



pl.figure()
pl.plot(trimmed_gyro_y)
pl.hlines(np.mean(trimmed_gyro_y), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_gyro_y)) + 'radians/sec')
pl.title('Raw gyro_y over time' + '\n stdev=' + str(np.std(trimmed_gyro_y))  + 'radians/sec')
pl.xlabel('Point number')
pl.ylabel('Gyro_y (radians/sec)')
pl.legend()

pl.figure()
pl.hist(trimmed_gyro_y - np.mean(trimmed_gyro_y))
pl.title('Histogram of gyro_y')
pl.xlabel('Gyro_y (radians/sec)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of gyro_y")



pl.figure()
pl.plot(trimmed_gyro_z)
pl.hlines(np.mean(trimmed_gyro_z), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_gyro_z)) + 'radians/sec')
pl.title('Raw gyro_z over time' + '\n stdev=' + str(np.std(trimmed_gyro_z))  + 'radians/sec')
pl.xlabel('Point number')
pl.ylabel('Gyro_z (radians/sec)')
pl.legend()



pl.figure()
pl.hist(trimmed_gyro_z - np.mean(trimmed_gyro_z))
pl.xlabel('Gyro_z (radians/sec)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of gyro_z")



# linear acceleration plots and calculations
pl.figure()
pl.plot(trimmed_lin_acc_x)
pl.hlines(np.mean(trimmed_lin_acc_x), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_lin_acc_x)) + 'm/s^2')
pl.title('Raw lin_acc_x over time' + '\n stdev=' + str(np.std(trimmed_lin_acc_x))  + 'm/s^2')
pl.xlabel('Point number')
pl.ylabel('lin_acc_x (m/sec^2)')
pl.legend()

pl.figure()
pl.hist(trimmed_lin_acc_x - np.mean(trimmed_lin_acc_x))
pl.xlabel('lin_acc_x (m/sec^2)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of lin_acc_x")

pl.figure()
pl.plot(trimmed_lin_acc_y)
pl.hlines(np.mean(trimmed_lin_acc_y), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_lin_acc_y)) + 'm/s^2')
pl.title('Raw lin_acc_y over time' + '\n stdev=' + str(np.std(trimmed_lin_acc_y))  + 'm/s^2')
pl.xlabel('Point number')
pl.ylabel('lin_acc_y (m/sec^2)')
pl.legend()

pl.figure()
pl.hist(trimmed_lin_acc_y - np.mean(trimmed_lin_acc_y))
pl.xlabel('lin_acc_y (m/sec^2)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of lin_acc_y")

pl.figure()
pl.plot(trimmed_lin_acc_z)
pl.hlines(np.mean(trimmed_lin_acc_z), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_lin_acc_z)) + 'm/s^2')
pl.title('Raw lin_acc_z over time' + '\n stdev=' + str(np.std(trimmed_lin_acc_z))  + 'm/s^2')
pl.xlabel('Point number')
pl.ylabel('lin_acc_z (m/sec^2)')
pl.legend()

pl.figure()
pl.hist(trimmed_lin_acc_z - np.mean(trimmed_lin_acc_z))
pl.xlabel('lin_acc_z (m/sec^2)')
pl.ylabel('Frequency')
pl.title("Frequency distribution of lin_acc_z")



# magnometer plots and calculations

pl.figure()
pl.plot(trimmed_mag_x)
pl.hlines(np.mean(trimmed_mag_x), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_mag_x)) + ' gauss')
pl.title('Raw mag_x over time' + '\n stdev=' + str(np.std(trimmed_mag_x))  + ' gauss')
pl.xlabel('Point number')
pl.ylabel('mag_x (gauss)')
pl.legend(loc='upper right')

pl.figure()
pl.hist(trimmed_mag_x - np.mean(trimmed_mag_x))
pl.title("Frequency distribution of raw mag_x")
pl.xlabel('mag_x (gauss)')
pl.ylabel('Frequency')

pl.figure()
pl.plot(trimmed_mag_y)
pl.hlines(np.mean(trimmed_mag_y), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_mag_y)) + ' gauss')
pl.title('Raw mag_y over time' + '\n stdev=' + str(np.std(trimmed_mag_y))  + ' gauss')
pl.xlabel('Point number')
pl.ylabel('mag_y (gauss)')
pl.legend(loc='upper right')

pl.figure()
pl.hist(trimmed_mag_y - np.mean(trimmed_mag_y))
pl.title("Frequency distribution of raw mag_y")
pl.xlabel('mag_y (gauss)')
pl.ylabel('Frequency')

pl.figure()
pl.plot(trimmed_mag_z)
pl.hlines(np.mean(trimmed_mag_z), -500, 3500, colors='orange', linestyles='dashed', label='mean=' + str(np.mean(trimmed_mag_z)) + ' gauss')
pl.title('Raw mag_z over time' + '\n stdev=' + str(np.std(trimmed_mag_z))  + ' gauss')
pl.xlabel('Point number')
pl.ylabel('mag_z (gauss)')
pl.legend(loc='upper right')

pl.figure()
pl.hist(trimmed_mag_z - np.mean(trimmed_mag_z))
pl.title("Frequency distribution of raw mag_z")
pl.xlabel('mag_z (gauss)')
pl.ylabel('Frequency')



pl.show()
 	