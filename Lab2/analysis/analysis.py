import matplotlib.pyplot as pl
import csv
import numpy as np
import math

from scipy import integrate
from scipy import signal

from tf.transformations import euler_from_quaternion


# Inspired by https://pythonprogramming.net/loading-file-data-matplotlib-tutorial/
# For magnometer calibration (https://www.fierceelectronics.com/components/compensating-for-tilt-hard-iron-and-soft-iron-effects)

x = []
y = []
seq = []
sample_points = []
ang_z = []

with open('mag.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    counter = 0;
    for row in plots:
    	if (counter > 905):
    		break
        x.append(float(row[4]))
        y.append(float(row[5]))
        seq.append(counter)
        sample_points.append(float(row[0]))
        counter = counter + 1

with open('imu.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    counter = 0;
    for row in plots:
    	if (counter > 905):
    		break
        ang_z.append(float(row[19]))
        counter = counter + 1

# pl.figure(0)
pl.plot(x,y, label='Magnetometer Data')
pl.xlabel('mag_x (Gauss)')
pl.ylabel('mag_y (Gauss)')


# hard iron adjustment

xmax = max(x)
xmin = min(x)
ymax = max(y)
ymin = min(y)

alpha = (xmax + xmin) / 2 # X axis offset
beta = (ymax + ymin) / 2 # Y axis offset

x_hard = []
y_hard = []

for i in range(len(x)):
	x_hard.append(x[i] - alpha)

for j in range(len(y)):
	y_hard.append(y[j] - beta)

# pl.figure(1)
pl.plot(x_hard, y_hard, label = "Magnetometer Data (corrected for hard-iron")
pl.xlabel('mag_x (Gauss)')
pl.ylabel('mag_y (Gauss)')


# print x_hard
# print y_hard


# soft-iron correction

max_r = 0
max_r_x1 = 0
max_r_y1 = 0
theta = 0


	
for i in range(len(x_hard)):
	if np.linalg.norm([x_hard[i], y_hard[i]]) > max_r:
		max_r = np.linalg.norm([x_hard[i], y_hard[i]])
		max_r_x1 = x_hard[i]
		max_r_y1 = y_hard[i] 

theta = np.arcsin(max_r_y1 / max_r)
c, s = np.cos(theta), np.sin(theta)
r = np.array(((c, s), (-s, c)))

x_hard_rotated = []
y_hard_rotated = []

for i in range(len(x_hard)):
	v = np.array((x_hard[i],y_hard[i]))
	x_hard_rotated_i, y_hard_rotated_i = r.dot(v)
	y_hard_rotated.append(y_hard_rotated_i)
	x_hard_rotated.append(x_hard_rotated_i)


q = max(y_hard_rotated)
r = max(x_hard_rotated)
scale_factor = q / r

x_hard_rotated_scaled = np.asarray(x_hard_rotated) * scale_factor

theta2 = -theta
c, s = np.cos(theta2), np.sin(theta2)
r = np.array(((c, s), (-s, c)))

x_corrected = []
y_corrected = []

for i in range(len(x_hard_rotated_scaled)):
	v = np.array((x_hard_rotated_scaled[i],y_hard_rotated[i]))
	x_corrected_i, y_corrected_i = r.dot(v)
	x_corrected.append(x_corrected_i)
	y_corrected.append(y_corrected_i)


pl.plot(x_corrected, y_corrected)


pl.title("Raw data (blue) with hard-iron(orange) and soft-iron (green) corrections")
pl.gca().set_aspect('equal')
pl.draw()

# yaw estimate

yaw_magnetometer = np.arctan2(np.negative(y_corrected), x_corrected)
unwrapped = np.unwrap(yaw_magnetometer)
unwrapped_zeroed = unwrapped - unwrapped[0]
pl.figure()
pl.plot(unwrapped_zeroed, label='Magnetometer') #calculated 


yaw_integrated_gyro = integrate.cumtrapz(ang_z, sample_points, initial=0)
yaw_integrated_gyro_scaled = yaw_integrated_gyro / 10**9
pl.plot(yaw_integrated_gyro_scaled, label='Integrated Yaw Rate') # integrated

# low-pass filter
sampling = 40.0
nyquist = sampling / 2
cutoff = 10
critical =  cutoff / nyquist
b, a = signal.butter(1, critical, 'low')
low_filtered_result = signal.filtfilt(b, a, unwrapped_zeroed)
# pl.figure()
# pl.plot(low_filtered_result)


# high-pass filter
sampling = 40.0
nyquist = sampling / 2
cutoff = .001
critical =  cutoff / nyquist
b, a = signal.butter(1, critical, 'high')
high_filterd_result = signal.filtfilt(b, a, yaw_integrated_gyro_scaled)
high_filterd_result_zeroed = high_filterd_result - high_filterd_result[0]
# pl.figure()
# pl.plot(high_filterd_result_zeroed)

# complementary filter
yaw_complementary_result = np.array(low_filtered_result)*.9 + np.array(high_filterd_result_zeroed)*.1 # relative strengths (.9 and .1)
# pl.figure()
pl.plot(yaw_complementary_result, label='Complementary Filter') # COMPLEMNETARY RESULT

# compare against raw value

q_x = []
q_y = []
q_z = []
q_w = []

raw_yaw = []

with open('Orientation_driving.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    counter = 0;
    for row in plots:
        if (counter > 905):
            break
        q_x.append(float(row[1]))
        q_y.append(float(row[2]))
        q_z.append(float(row[3]))
        q_w.append(float(row[4]))
        counter = counter + 1

for i in range(len(q_x)):
    ro, pi, ya = euler_from_quaternion([q_x[i], q_y[i], q_z[i], q_w[i]])
    raw_yaw.append(ya)

# pl.figure()
pl.plot(np.radians(np.unwrap(raw_yaw)), label="Raw Reading") # raw_yaw

pl.title('Comparing different means to calculate Yaw (radians)')
pl.xlabel('Point number')
pl.ylabel('Yaw (radians)')
pl.legend()

pl.show()