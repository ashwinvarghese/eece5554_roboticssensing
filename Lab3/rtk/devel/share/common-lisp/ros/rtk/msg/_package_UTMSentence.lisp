(cl:in-package rtk-msg)
(cl:export '(HEADER-VAL
          HEADER
          LATITUDE-VAL
          LATITUDE
          LONGITUDE-VAL
          LONGITUDE
          ALTITUDE-VAL
          ALTITUDE
          UTM_EASTING-VAL
          UTM_EASTING
          UTM_NORTHING-VAL
          UTM_NORTHING
          ZONE-VAL
          ZONE
          FIX-VAL
          FIX
          LETTER-VAL
          LETTER
))