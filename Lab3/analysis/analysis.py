import matplotlib.pyplot as pl
import csv
import numpy as np
import math

from scipy import integrate
from scipy import signal
from mpl_toolkits.mplot3d import Axes3D


# 3d scatter code inspired by https://pythonprogramming.net/matplotlib-3d-scatterplot-tutorial/

def convert_latitude(field):
    """Convert a latitude string to floating point decimal degrees.
    Args:
        field (str): Latitude string, expected to be formatted as DDMM.MMM, where
            DD is the latitude degrees, and MM.MMM are the minutes latitude.
    Returns:
        Floating point latitude in decimal degrees.
    """
    return float(field[0:2]) + float(field[2:]) / 60.0

def convert_longitude(field):
    """Convert a longitude string to floating point decimal degrees.
    Args:
        field (str): Longitude string, expected to be formatted as DDDMM.MMM, where
            DDD is the longitude degrees, and MM.MMM are the minutes longitude.
    Returns:
        Floating point latitude in decimal degrees.
    """
    return float(field[0:2]) + float(field[2:]) / 60.0


def normalize(values):
    avg_x = np.average(values)
    return [val - avg_x for val in values]

# partial stat occlusions

x = []
y = []
utm_easting = []
utm_northing = []
z = []
time = []

import utm

with open('../data/Lab3_isec_stat.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        if ((int(row[10])) == 5) : # filter on float
            time.append(float(row[5]))
            x.append(convert_latitude(row[6]))
            y.append(convert_longitude(row[7]))
            z.append(float(row[13]))


for i in range(len(x)):
    utm_easting.append(utm.from_latlon(x[i], y[i])[0])
    utm_northing.append(utm.from_latlon(x[i], y[i])[1])



pl.scatter(normalize(utm_easting), normalize(utm_northing))
pl.title('2D Scatter plot of normalized, stationary + occluded data')
pl.xlabel('Easting (meters)')
pl.ylabel('Northing (meters)')


fig = pl.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(normalize(utm_easting), normalize(utm_northing), normalize(z), c='r', marker='o')

ax.set_xlabel('Easting (meters)')
ax.set_ylabel('Northing (meters)')
ax.set_zlabel('Altitude (meters)')
ax.set_title('3D Scatter plot of normalized, stationary + occluded data')

## noise easting, northing, altitude)
pl.figure()
pl.hist(normalize(utm_easting))
pl.title('Frequency distriubtion of UTM_Easting normalized, stationary + occluded data')
pl.xlabel("UTM Easting (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(utm_northing))
pl.title('Frequency distriubtion of UTM_Northing normalized, stationary + occluded data')
pl.xlabel("UTM Northing (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(z))
pl.title('Frequency distriubtion of altitude normalized, stationary + occluded data')
pl.xlabel("Altitude (meters)")
pl.ylabel('Frequency (count)')

print np.std(utm_easting)
print np.std(utm_northing)
print np.std(z)

# open stationary

x = []
y = []
utm_easting = []
utm_northing = []
z = []
time = []

import utm

with open('../data/Lab3_open_stat.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        if ((int(row[10])) == 5 or (int(row[10])) == 4) : # filter on float or fix
            time.append(float(row[5]))
            x.append(convert_latitude(row[6]))
            y.append(convert_longitude(row[7]))
            z.append(float(row[13]))


for i in range(len(x)):
    utm_easting.append(utm.from_latlon(x[i], y[i])[0])
    utm_northing.append(utm.from_latlon(x[i], y[i])[1])

pl.figure()
pl.scatter(normalize(utm_easting), normalize(utm_northing))
pl.title('2D Scatter plot of normalized, stationary + open data')
pl.xlabel('Easting (meters)')
pl.ylabel('Northing (meters)')


fig = pl.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(normalize(utm_easting), normalize(utm_northing), normalize(z), c='g', marker='o')

ax.set_xlabel('Easting (meters)')
ax.set_ylabel('Northing (meters)')
ax.set_zlabel('Altitude (meters)')
ax.set_title('3D Scatter plot of normalized, stationary + open data')

## noise easting, northing, altitude)
pl.figure()
pl.hist(normalize(utm_easting))
pl.title('Frequency distriubtion of UTM_Easting normalized, stationary + open data')
pl.xlabel("UTM Easting (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(utm_northing))
pl.title('Frequency distriubtion of UTM_Northing normalized, stationary + open data')
pl.xlabel("UTM Northing (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(z))
pl.title('Frequency distriubtion of altitude normalized, stationary + open data')
pl.xlabel("Altitude (meters)")
pl.ylabel('Frequency (count)')


print np.std(utm_easting)
print np.std(utm_northing)
print np.std(z)

print "MEAN", np.mean(np.sqrt(np.square(normalize(utm_easting)) + np.square(normalize(utm_northing))))

# moving occluded

x = []
y = []
utm_easting = []
utm_northing = []
z = []
time = []

import utm

with open('../data/Lab3_isec_moving.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        if ((int(row[10])) == 5) : # filter on float
            time.append(float(row[5]))
            x.append(convert_latitude(row[6]))
            y.append(convert_longitude(row[7]))
            z.append(float(row[13]))


for i in range(len(x)):
    utm_easting.append(utm.from_latlon(x[i], y[i])[0])
    utm_northing.append(utm.from_latlon(x[i], y[i])[1])


pl.figure()
pl.scatter(normalize(utm_easting), normalize(utm_northing))
pl.title('2D Scatter plot of normalized, moving + occluded data')
pl.xlabel('Easting (meters)')
pl.ylabel('Northing (meters)')


fig = pl.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(normalize(utm_easting), normalize(utm_northing), normalize(z), c='orange', marker='o')

ax.set_xlabel('Easting (meters)')
ax.set_ylabel('Northing (meters)')
ax.set_zlabel('Altitude (meters)')
ax.set_title('3D Scatter plot of normalized, moving + occluded data')

## noise easting, northing, altitude)
pl.figure()
pl.hist(normalize(utm_easting))
pl.title('Frequency distriubtion of UTM_Easting normalized, moving + occluded data')
pl.xlabel("UTM Easting (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(utm_northing))
pl.title('Frequency distriubtion of UTM_Northing normalized, moving + occluded data')
pl.xlabel("UTM Northing (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(z))
pl.title('Frequency distriubtion of altitude normalized, moving + occluded data')
pl.xlabel("Altitude (meters)")
pl.ylabel('Frequency (count)')

print np.std(utm_easting)
print np.std(utm_northing)
print np.std(z)


# open  moving

x = []
y = []
utm_easting = []
utm_northing = []
z = []
time = []

import utm

with open('../data/Lab3_movingop.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    next(plots) # first line is field names
    for row in plots:
        if ((int(row[10])) == 5 or (int(row[10])) == 4) : # filter on float or fix
            time.append(float(row[5]))
            x.append(convert_latitude(row[6]))
            y.append(convert_longitude(row[7]))
            z.append(float(row[13]))


for i in range(len(x)):
    utm_easting.append(utm.from_latlon(x[i], y[i])[0])
    utm_northing.append(utm.from_latlon(x[i], y[i])[1])

pl.figure()
pl.scatter(normalize(utm_easting), normalize(utm_northing))
pl.title('2D Scatter plot of normalized, moving + open data')
pl.xlabel('Easting (meters)')
pl.ylabel('Northing (meters)')


fig = pl.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(normalize(utm_easting), normalize(utm_northing), normalize(z), c='brown', marker='o')

ax.set_xlabel('Easting (meters)')
ax.set_ylabel('Northing (meters)')
ax.set_zlabel('Altitude (meters)')
ax.set_title('3D Scatter plot of normalized, moving + open data')

## noise easting, northing, altitude)
pl.figure()
pl.hist(normalize(utm_easting))
pl.title('Frequency distriubtion of UTM_Easting normalized, moving + open data')
pl.xlabel("UTM Easting (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(utm_northing))
pl.title('Frequency distriubtion of UTM_Northing normalized, moving + open data')
pl.xlabel("UTM Northing (meters)")
pl.ylabel('Frequency (count)')

pl.figure()
pl.hist(normalize(z))
pl.title('Frequency distriubtion of altitude normalized, moving + open data')
pl.xlabel("Altitude (meters)")
pl.ylabel('Frequency (count)')

print np.std(utm_easting)
print np.std(utm_northing)
print np.std(z)

pl.show()




