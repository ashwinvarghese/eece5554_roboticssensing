from scipy import stats

import pandas as pd
import plotly.express as px
import numpy as np

df = pd.read_csv('dataStraight.csv')
dfNorm = {}
dfNorm['field.utm_easting'] = []
dfNorm['field.utm_northing'] = []
dfNorm['time'] = []


easting_min = np.amin(df['field.utm_easting'])
northing_min = np.amin(df['field.utm_northing'])
time_min = np.amin(df['field.header.stamp'])

for i in range (len(df['field.utm_easting'])):
	dfNorm['field.utm_easting'].append(float(df['field.utm_easting'][i]) - float(easting_min))

for i in range (len(df['field.utm_northing'])):
	dfNorm['field.utm_northing'].append(float(df['field.utm_northing'][i]) - float(northing_min))
for i in range (len(df['field.header.stamp'])):
	dfNorm['time'].append(float(df['field.header.stamp'][i]) - float(time_min))	

easting_min = np.amin(dfNorm['field.utm_easting'])
northing_min = np.amin(dfNorm['field.utm_northing'])
time_min = np.amin(dfNorm['time'])

# fig = px.scatter(dfNorm, x="field.utm_easting", y="field.utm_northing")
# fig = px.scatter(dfNorm, x="time", y="field.utm_northing")
fig = px.scatter(dfNorm, x="time", y="field.utm_easting")

fig.show()

print stats.linregress(dfNorm['field.utm_easting'],dfNorm['field.utm_northing'])
print np.mean(df['field.utm_easting']), np.mean(df['field.utm_northing'])
print np.std(df['field.utm_easting']), np.std(df['field.utm_northing'])
