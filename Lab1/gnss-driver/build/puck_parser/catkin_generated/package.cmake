set(_CATKIN_CURRENT_PACKAGE "puck_parser")
set(puck_parser_VERSION "0.0.0")
set(puck_parser_MAINTAINER "ashwin <varghese.a@husky.neu.edu>")
set(puck_parser_PACKAGE_FORMAT "2")
set(puck_parser_BUILD_DEPENDS "message_generation" "rospy")
set(puck_parser_BUILD_EXPORT_DEPENDS "rospy")
set(puck_parser_BUILDTOOL_DEPENDS "catkin")
set(puck_parser_BUILDTOOL_EXPORT_DEPENDS )
set(puck_parser_EXEC_DEPENDS "message_runtime" "rospy")
set(puck_parser_RUN_DEPENDS "message_runtime" "rospy")
set(puck_parser_TEST_DEPENDS )
set(puck_parser_DOC_DEPENDS )
set(puck_parser_URL_WEBSITE "")
set(puck_parser_URL_BUGTRACKER "")
set(puck_parser_URL_REPOSITORY "")
set(puck_parser_DEPRECATED "")