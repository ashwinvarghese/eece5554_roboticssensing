# CMake generated Testfile for 
# Source directory: /home/ashwin/Documents/eece5554_roboticssensing/Lab1/gnss-driver/src
# Build directory: /home/ashwin/Documents/eece5554_roboticssensing/Lab1/gnss-driver/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("bag2mat")
subdirs("puck_parser")
