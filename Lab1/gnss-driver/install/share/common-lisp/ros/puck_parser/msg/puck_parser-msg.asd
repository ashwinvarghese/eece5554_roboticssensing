
(cl:in-package :asdf)

(defsystem "puck_parser-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "UTMSentence" :depends-on ("_package_UTMSentence"))
    (:file "_package_UTMSentence" :depends-on ("_package"))
  ))