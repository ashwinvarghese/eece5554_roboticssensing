#!/usr/bin/env python

# Adapted from https://github.com/ros-drivers/nmea_navsat_driver/blob/master/src/libnmea_navsat_driver/nodes/nmea_topic_serial_reader.py

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, Eric Perko
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the names of the authors nor the names of their
#    affiliated organizations may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import serial

# from nmea_msgs.msg import Sentence
import rospy
import gnssParser as gnssParser
import utm

# from libnmea_navsat_driver.driver import RosNMEADriver
from puck_parser.msg import UTMSentence
 

def main():
    """Create and run the nmea_topic_serial_reader ROS node.
    Opens a serial device and publishes data from the device as nmea_msgs.msg.Sentence messages.
    ROS parameters:
        ~port (str): Path of the serial device to open.
        ~baud (int): Baud rate to configure the serial device.
    ROS publishers:
        nmea_sentence (nmea_msgs.msg.Sentence): Publishes each line from the open serial device as a new
            message. The header's stamp is set to the rostime when the data is read from the serial device.
    """
    rospy.init_node('nmea_topic_serial_reader')
    r = rospy.Rate(10)	
    nmea_pub = rospy.Publisher("nmea_sentence", UTMSentence, queue_size=1)

    serial_port = rospy.get_param('~port', '/dev/ttyACM0')
    serial_baud = rospy.get_param('~baud', 4800)

    try:
        GPS = serial.Serial(port=serial_port, baudrate=serial_baud, timeout=2)
        while not rospy.is_shutdown():
            unparsed_nmea_sentence = GPS.readline().strip()

            parsed_sentence = {}

            # unparsed_nmea_sentence = GPS.readline()
            # unparsed_nmea_sentence = "$GPGGA,183730,3907.356,N,12102.482,W,1,05,1.6,646.4,M,-24.1,M,,*75"

            if (unparsed_nmea_sentence.startswith("$GPGGA")):
	        	parsed_sentence = gnssParser.parse_nmea_sentence(unparsed_nmea_sentence)

	        	# parsed_sentence = gnssParser.parse_nmea_sentence(unparsed_nmea_sentence)
	        	data = parsed_sentence['GGA']

	        	utm_data = utm.from_latlon(data['latitude'], data['longitude'])
	        	easting = utm_data[0] # easting
	        	northing = utm_data[1] # northing
	        	zone_number = utm_data[2] # zone_number
	        	zone_letter = utm_data[3] # zone_letter

	        	print data

	        	msg = UTMSentence();
	        	msg.header.stamp = rospy.get_rostime()
	        	msg.latitude = data['latitude']
	        	msg.longitude = data['longitude']
	        	msg.altitude = data['altitude']
	        	msg.utm_easting = easting
	        	msg.utm_northing = northing
	        	msg.zone = zone_number
	        	msg.letter =zone_letter

	        	nmea_pub.publish(msg)
	        	r.sleep()

    except rospy.ROSInterruptException:
        GPS.close()  # Close GPS serial port

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass